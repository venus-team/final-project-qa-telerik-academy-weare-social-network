# List of test cases

## SUU_ Unregistered user

**Happy path**  

- [ ] H_001. Unregistered user should be able to view a list of all public profiles

- [ ] H_002. Unregistered user should be able to search for a public profile based on name  

- [ ] H_003. Unregistered user should be able to search for a public profile based on profession 

- [ ] H_004. Unregistered user should be able to see picture of users that have set their picture visibility to Public  

- [ ] H_005. Unregistered user should not be able to see picture of users that have set their picture visibility to Private

- [ ] H_006. Unregistered user should be able to view a list of latest public posts

- [ ] H_007. Unregistered user should be able to open public posts when post is selected

- [ ] H_008. Unregistered user should be able to view a list of public posts of registered user ordered chronologically

- [ ] H_009. Unregistered user should be able to view comments of public post
- [ ] H_010. Unregistered user should be able to register in application when fill in registration form with valid data

**Negative**

- [ ] U_001. Unregistered user should receive warning message when fill in registration form with invalid data

- [ ] U_002. Unregistered user should receive warning message when fill in registration form with valid data and different passwords


## SRU_ Registered user

**Happy path**  
**_Profile section:_**   

- [ ] H_001. Registered user should be able to log in to application

- [ ] H_002. Registered user should be able to edit his personal information with valid data

- [ ] H_003. Registered user should be able to edit with valid data his professional information

- [ ] H_004. Registered user should be able to edit with valid data services he offered

- [ ] H_005. Registered user should be able to edit with valid data Personal info & Safety

- [ ] H_006. Registered user should be able to Edit Name

- [ ] H_007. Registered user should be able to upload new profile picture selected from the computer

- [ ] H_008. Registered user should be able to change profile picture visibility   

- [ ] H_009. Registered user should be able to edit email  
- [ ] H_010. Registered user should be able to change the gender
- [ ] H_025. Registered user should be able to log out

**_Connection section:_**  

- [ ] H_011. Registered user should be able to send connection requests to other users  

- [ ] H_012. Registered user should be able to accept users connect requests when connection request is send

- [ ] H_013. Registered user should be able to decline users connect requests when connection request is send 

- [ ] H_014. Registered user should be able to disconnect with connected users 
- [ ] H_015. Disconnect should not need approval when user is been disconnected  


**_Posts section:_**   

- [ ] H_016. Registered user should be able to create new post  

- [ ] H_017. Registered user should be able to like post 

- [ ] H_018. Registered user should be able to unlike post  
- [ ] H_022. Registered user should be able to see all own posts when "Latest activity"   tab is clicked in "Personal Profile"  

- [ ] H_023. Registered user should be able to edit own post 
- [ ] H_024. Registered user should be able to delete own posts

**_Comments section:_**

- [ ] H_019. Registered user should be able to create new comment

- [ ] H_020. Registered user should be able to like comment  

- [ ] H_021. Registered user should be able to unlike comment  
 
**Negative**
  
- [ ] U_026. Registered user should receive warning message when fill in his personal information with invalid data  



## SAU_ System administrator  (inherits all registered user rights)

**Happy path**

- [ ] H_001. Administrator should be able to edit public user's profiles

- [ ] H_002. Administrator should be able to edit private user's profiles

- [ ] H_003. Administrator should be able to delete public user's profiles

- [ ] H_004. Administrator should be able to delete private user's profiles

- [ ] H_005. Administrator should be able to edit public user's posts

- [ ] H_006. Administrator should be able to edit private user's posts

- [ ] H_007. Administrator should be able to delete public user's posts

- [ ] H_008. Administrator should be able to delete private user's posts

- [ ] H_009. Administrator should be able to edit user's comments


**Negative**


- [ ] U_001. Administrator should receive warning message when edit user's personal information with invalid data

- [ ] U_002. Administrator should receive warning message when edit with invalid data user's professional information

- [ ] U_003. Administrator should receive warning message when edit with invalid data user's offered services

- [ ] U_004. Administrator should receive warning message when edit with invalid data user's Personal info & Safety
- [ ] H_005. Administrator should receive warning message when edit public user's posts with invalid data
