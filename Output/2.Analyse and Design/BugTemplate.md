## Summary:

(Summarize the bug encountered concisely)

## Prerquites:

(Configuration needed for the test; Additional data that needs to be entered beforehand; Presets of any kind)

## Steps to reproduce:

(How one can reproduce the issue step by step)

1. ....;

2. ....;

3. ....;

4. .....

## Expected result:

(What do you expect to happen after following the steps)

## Actual result:

(What really happened)

## Severity:

(Depending on the impact on the functionality or further testing.
Possible severity options can be - Blocking​, Critical​, High, Medium​, Low)

## Additional info:

(Environment used for the test; Used web browser; OS; ets.)

## Relevant logs and/or screenshots:

(Attached images/videos; Attached stack trace; Attached code snippets; Everything that can help reproducing or fixing the defect; Paste any relevant logs - please use code blocks (```) to format console output, logs, and code, as it's very hard to read otherwise.)

