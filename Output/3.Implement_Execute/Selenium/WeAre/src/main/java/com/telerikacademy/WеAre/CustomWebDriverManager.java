package com.telerikacademy.WеAre;

import io.github.bonigarcia.wdm.managers.ChromeDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


public class CustomWebDriverManager {
	public enum CustomWebDriverManagerEnum {
		INSTANCE;
		private WebDriver driver = setupBrowser();

		private WebDriver setupBrowser(){
//			FirefoxDriverManager.getInstance().setup();
//			WebDriver firefoxDriver = new FirefoxDriver();
//			firefoxDriver.manage().window().maximize();
//			driver = firefoxDriver;
//			return firefoxDriver;
			ChromeOptions opts = new ChromeOptions();
			opts.addArguments("-incognito");
			ChromeDriverManager.chromedriver().setup();
			driver = new ChromeDriver(opts);
			driver.manage().window().maximize();
			return driver;
		}

		public void quitDriver() {
			if (driver != null) {
				driver.quit();
				driver = null;
			}
		}

		public WebDriver getDriver() {
			if (driver == null){
				setupBrowser();
			}
			return driver;
		}


	}
}
