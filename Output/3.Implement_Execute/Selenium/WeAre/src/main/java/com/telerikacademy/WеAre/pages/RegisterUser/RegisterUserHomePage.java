package com.telerikacademy.WеAre.pages.RegisterUser;

import com.telerikacademy.WеAre.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class RegisterUserHomePage extends BasePage {
    public RegisterUserHomePage(WebDriver driver) {
        super(driver, "baseUrl");
    }

    public void navigateToPersonalProfile(){
        actions.waitForElementClickable("registerUserHomePage.personalProfileBnt");
        actions.clickElementJS("registerUserHomePage.personalProfileBnt");

        assertPersonalProfileIsNavigated();
    }

    public void navigateToCreateNewPostPage(){
        actions.waitForElementClickable("registerUserHomePage.addNewPostBnt");
        actions.clickElementJS("registerUserHomePage.addNewPostBnt");

        assertCreateNewPostPageIsNavigated();
    }

    public void logOut(){

        actions.waitForElementClickable("registerUserHomePage.homeBnt");
        actions.clickElementJS("registerUserHomePage.homeBnt");
        actions.waitForElementClickable("registerUserHomePage.logoutBnt");

        actions.clickElementJS("registerUserHomePage.logoutBnt");
    }

//*************ASSERTIONS*****************************

    public void assertPersonalProfileIsNavigated(){
        actions.waitForElementClickable("userProfilePage.editProfileBnt");
        actions.assertElementPresent("userProfilePage.editProfileBnt");
    }

    public void assertCreateNewPostPageIsNavigated(){
        actions.waitForElementClickable("newPostPage.createNewPostHeader");
        actions.assertElementPresent("newPostPage.createNewPostHeader");
    }

}
