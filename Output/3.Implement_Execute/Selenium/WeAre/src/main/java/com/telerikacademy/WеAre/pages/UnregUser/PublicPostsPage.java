package com.telerikacademy.WеAre.pages.UnregUser;

import com.telerikacademy.WеAre.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class PublicPostsPage extends BasePage {
    public PublicPostsPage(WebDriver driver) {
        super(driver, "postsUrl");
    }

    public void ExploreFirstPostFromPublicPosts(){
        assertLatestPostsArePresent();
        actions.waitForElementClickable("publicPostsPage.firstPostExplorePostBtn");
        actions.clickElementJS("publicPostsPage.firstPostExplorePostBtn");
    }

    public void browseAllPublicPosts(){
        actions.waitForElementClickable("publicPostsPage.browseAllPublicPostsBnt");
        actions.clickElementJS("publicPostsPage.browseAllPublicPostsBnt");
    }

    public void postComment(String message){
        //Write comment text
        actions.waitForElementClickable("publicPostsPage.commentMessageInput");
        actions.clearFieldText("publicPostsPage.commentMessageInput");
        actions.typeValueInField(message , "publicPostsPage.commentMessageInput");

        //Click on "Post comment" button
        actions.waitForElementClickable("publicPostsPage.postCommentBnt");
        actions.clickElementJS("publicPostsPage.postCommentBnt");
    }

    public void showComments(){
        actions.waitForElementClickable("publicPostsPage.showCommentsBnt");
        actions.clickElementJS("publicPostsPage.showCommentsBnt");

        assertCommentsAreVisible();
    }

    public void likePost(){
        actions.waitForElementClickable("publicPostsPage.likePostBnt");
        actions.clickElementJS("publicPostsPage.likePostBnt");
    }

    public void dislikePost(){
        actions.waitForElementClickable("publicPostsPage.dislikePostBnt");
        actions.clickElementJS("publicPostsPage.dislikePostBnt");
    }

    public void likeComment(){
        actions.waitForElementClickable("publicPostsPage.likeCommentBnt");
        actions.clickElementJS("publicPostsPage.likeCommentBnt");
    }

    public void dislikeComment(){
        actions.waitForElementClickable("publicPostsPage.dislikeCommentBnt");
        actions.clickElementJS("publicPostsPage.dislikeCommentBnt");
    }

    //****************** ASSERTIONS ***********************

    public void assertLatestPostsArePresent(){
        actions.waitForElementClickable("publicPostsPage.latestPostsPageHeader");
        actions.assertElementPresent("publicPostsPage.latestPostsPageHeader");
    }

    public void assertCommentsAreVisible(){
        actions.waitForElementClickable("publicPostsPage.commentsVisible");
        actions.assertElementPresent("publicPostsPage.commentsVisible");
    }
}
