package com.telerikacademy.WеAre.pages.RegisterUser;

import com.telerikacademy.WеAre.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class PersonalProfilePage extends BasePage {
    public PersonalProfilePage(WebDriver driver) {
        super(driver, "baseUrl");
    }

    public void editPersonalInformation(String firstName, String lastName, String birthdayDay, String birthdayMonth, String birthdayYear , String gender, String email, String city){
        openProfileForEdition();
        actions.waitForElementClickable("userProfilePage.firstNameInput");
        actions.clearFieldText("userProfilePage.firstNameInput");
        actions.typeValueInField(firstName, "userProfilePage.firstNameInput");

        actions.waitForElementClickable("userProfilePage.lastNameInput");
        actions.clearFieldText("userProfilePage.lastNameInput");
        actions.typeValueInField(lastName, "userProfilePage.lastNameInput");

        actions.waitForElementClickable("userProfilePage.birthdayInput");
        actions.clearFieldText("userProfilePage.birthdayInput");
        actions.typeValueInField(birthdayDay, "userProfilePage.birthdayInput");
        actions.typeValueInField(birthdayMonth, "userProfilePage.birthdayInput");
        actions.typeValueInField(birthdayYear, "userProfilePage.birthdayInput");

        actions.waitForElementClickable("userProfilePage.genderInput");
        actions.clickElement("userProfilePage.genderInput");
        actions.selectValueFromDropDown(gender , "userProfilePage.genderInput");

        actions.waitForElementClickable("userProfilePage.emailInput");
        actions.clearFieldText("userProfilePage.emailInput");
        actions.typeValueInField(email, "userProfilePage.emailInput");

        actions.waitForElementClickable("userProfilePage.fewWordsInput");
        actions.clearFieldText("userProfilePage.fewWordsInput");
        actions.typeValueInField(firstName, "userProfilePage.fewWordsInput");

        actions.waitForElementClickable("userProfilePage.cityInput");
        actions.clickElement("userProfilePage.cityInput");
        actions.selectValueFromDropDown(city , "userProfilePage.cityInput");

        actions.waitForElementClickable("userProfilePage.updateMyProfileBnt");
        actions.clickElement("userProfilePage.updateMyProfileBnt");
    }

    public void editProfessionalInformation(String profession){
        openProfileForEdition();

        actions.waitForElementClickable("userProfilePage.professionInput");
        actions.clickElement("userProfilePage.professionInput");
        actions.selectValueFromDropDown(profession , "userProfilePage.professionInput");

        actions.waitForElementClickable("userProfilePage.updateProfessionalInformationBnt");
        actions.clickElement("userProfilePage.updateProfessionalInformationBnt");
    }

    public void editServicesInformation(String skill, String availability){
        openProfileForEdition();

        actions.waitForElementClickable("userProfilePage.skillInput");
        actions.clearFieldText("userProfilePage.skillInput");
        actions.typeValueInField(skill , "userProfilePage.skillInput");

        actions.waitForElementClickable("userProfilePage.availabilityInput");
        actions.clearFieldText("userProfilePage.availabilityInput");
        actions.typeValueInField(availability , "userProfilePage.availabilityInput");

        actions.waitForElementClickable("userProfilePage.updateSkillsInformationBnt");
        actions.clickElement("userProfilePage.updateSkillsInformationBnt");
    }

    public void editPictureAndSafetyInformation(String filePath, String privacy){
        openProfileForEdition();

        actions.waitForElementClickable("userProfilePage.chooseFileBnt");
        actions.fileUpload(filePath,"userProfilePage.chooseFileBnt");

        actions.waitForElementClickable("userProfilePage.picturePrivacy");
        actions.clickElement("userProfilePage.picturePrivacy");
        actions.selectValueFromDropDown(privacy , "userProfilePage.picturePrivacy");

        actions.waitForElementClickable("userProfilePage.updatePictureAndPrivacyBnt");
        actions.clickElement("userProfilePage.updatePictureAndPrivacyBnt");
    }

    public void openProfileForEdition(){
        actions.waitForElementClickable("userProfilePage.editProfileBnt");

        actions.clickElementJS("userProfilePage.editProfileBnt");


        assertProfileIsOpenForEdition();
    }

    public void clickOnNewFriendsRequestsBnt(){
        actions.waitForElementClickable("userProfilePage.newFriendRequestBnt");
        actions.clickElementJS("userProfilePage.newFriendRequestBnt");
    }

    public void clickOnFirstPendingRequestAcceptBnt(){
        actions.waitForElementClickable("userProfilePage.approveRequestBnt");
        actions.clickElementJS("userProfilePage.approveRequestBnt");
    }

    public void clickOnFirstPendingRequestDeclineBnt(){
        actions.waitForElementClickable("userProfilePage.declineRequestBnt");
        actions.clickElementJS("userProfilePage.declineRequestBnt");
    }

    //*********ASSERTIONS*******

    public void assertProfileIsOpenForEdition(){
        actions.waitForElementClickable("userProfilePage.editPersonalInformationBnt");
        actions.assertElementPresent("userProfilePage.editPersonalInformationBnt");
    }
}
