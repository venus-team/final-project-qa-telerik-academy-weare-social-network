package com.telerikacademy.WеAre.pages.UnregUser;

import org.openqa.selenium.WebDriver;
import com.telerikacademy.WеAre.pages.BasePage;

public class HomePage extends BasePage {

    public HomePage(WebDriver driver) {
        super(driver, "baseUrl");
    }

    public void searchWithoutTerm(){
        actions.waitForElementClickable("homePage.searchButton");
        actions.clickElement("homePage.searchButton");
    }

    public void searchAndSubmitByName(String term) {
        actions.waitForElementClickable("homePage.searchByNameInput");
        actions.typeValueInField(term, "homePage.searchByNameInput");
        actions.waitFor(1000);
        searchWithoutTerm();
    }

    public void searchAndSubmitByProffesion(String term) {
        actions.waitForElementClickable("homePage.searchByProfessionInput");
        actions.typeValueInField(term, "homePage.searchByProfessionInput");
        actions.waitFor(1000);
        searchWithoutTerm();
    }

    public void navigateToLatestPostsPage(){
        actions.waitForElementClickable("homePage.latestPostsButton");
        actions.clickElementJS("homePage.latestPostsButton");

        assertLatestPostsPageNavigated();
    }

    public void clickOnRegisterBnt(){
        actions.waitForElementClickable("homePage.RegisterButton");
        actions.clickElementJS("homePage.RegisterButton");
    }

    //********** ASSERTIONS **************
    public void assertSearchInputsPresent() {

        actions.assertElementPresent("homePage.searchByNameInput");
        actions.assertElementPresent("homePage.searchByProfessionInput");
    }

    public void assertLatestPostsPageNavigated() {

        actions.waitForElementVisible("publicPostsPage.latestPostsPageHeader");
        actions.assertElementPresent("publicPostsPage.latestPostsPageHeader");
    }
}
