package com.telerikacademy.WеAre.pages.RegisterUser;

import com.telerikacademy.WеAre.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class CreatePostPage extends BasePage {
    public CreatePostPage(WebDriver driver) {
        super(driver, "newPostUrl");
    }

    public void createPost(String privacy, String message, String filePath){
        //Choose privacy
        actions.waitForElementClickable("newPostPage.privacyDropDown");
        actions.clickElement("newPostPage.privacyDropDown");
        actions.selectValueFromDropDown(privacy , "newPostPage.privacyDropDown");

        //Write message
        actions.waitForElementClickable("newPostPage.messageInput");
        actions.clearFieldText("newPostPage.messageInput");
        actions.typeValueInField(message , "newPostPage.messageInput");

        //Select file
        actions.waitForElementClickable("newPostPage.imageUploadBnt");
        actions.fileUpload(filePath, "newPostPage.imageUploadBnt");

        //Click on Save post button
        actions.waitForElementClickable("newPostPage.savePostBnt");
        actions.clickElement("newPostPage.savePostBnt");
    }
}
