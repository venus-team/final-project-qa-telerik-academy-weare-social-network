package com.telerikacademy.WеAre.pages.UnregUser;

import com.telerikacademy.WеAre.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class SignInPage extends BasePage {
    public SignInPage(WebDriver driver) {
        super(driver, "signinUrl");
    }

    public void signInUser(String userName, String pass){
        navigateToPage();
        actions.waitForElementClickable("signInPage.userNameInput");
        actions.typeValueInField(userName, "signInPage.userNameInput");
        actions.waitForElementClickable("signInPage.userPasswordInput");
        actions.typeValueInField(pass, "signInPage.userPasswordInput");
        actions.waitForElementClickable("signInPage.loginBnt");
        actions.clickElement("signInPage.loginBnt");
    }

}
