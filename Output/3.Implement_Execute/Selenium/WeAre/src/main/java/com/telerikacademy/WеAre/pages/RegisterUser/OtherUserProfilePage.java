package com.telerikacademy.WеAre.pages.RegisterUser;

import com.telerikacademy.WеAre.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class OtherUserProfilePage extends BasePage {
    public OtherUserProfilePage(WebDriver driver) {
        super(driver, "baseUrl");
    }

    public void clickOnConnectBnt(){
        actions.waitForElementClickable("otherUserProfilePage.connectBnt");
        actions.clickElement("otherUserProfilePage.connectBnt");
    }

    public void clickOnDisconnectBnt(){
        actions.waitForElementClickable("otherUserProfilePage.disconnectBnt");
        actions.clickElement("otherUserProfilePage.disconnectBnt");
    }


}
