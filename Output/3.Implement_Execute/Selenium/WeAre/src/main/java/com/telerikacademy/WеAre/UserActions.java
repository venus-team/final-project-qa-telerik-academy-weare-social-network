package com.telerikacademy.WеAre;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class UserActions {
    public WebDriver getDriver() {
        return driver;
    }

    final WebDriver driver;

    public UserActions() {
        this.driver = Utils.getWebDriver();
    }

    public static void loadBrowser(String baseUrlKey) {
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey(baseUrlKey));
    }

    public static void quitDriver() {
        Utils.tearDownWebDriver();
    }

    public void clickElement(String key, Object... arguments) {
        String locator = getLocatorValueByKey(key, arguments);

        Utils.LOG.info("Clicking on element " + key);
        WebElement element = driver.findElement(By.xpath(locator));
        Actions actions = new Actions(driver);
        actions.moveToElement(element).click().build().perform();
    }

    public void clickElementJS (String locator){
        Utils.LOG.info("Clicking on element " + locator);
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);

    }


    public void hoverElement(String key, Object... arguments) {

        String locator = getLocatorValueByKey(key, arguments);
        Utils.LOG.info("Hover on element " + key);
        WebElement element = driver.findElement(By.xpath(locator));

        Actions action = new Actions(driver);
        action.moveToElement(element).perform();
    }

    public WebElement getElement(String key, Object... arguments){
        String locator = getLocatorValueByKey(key, arguments);
        Utils.LOG.info("Get element " + key);
        return driver.findElement(By.xpath(locator));
    }

    public String getElementText(String key, Object... arguments){
        String locator = getLocatorValueByKey(key, arguments);
        Utils.LOG.info("Get element " + key);
        return driver.findElement(By.xpath(locator)).getText();
    }

    public void dragAndDropElement(WebElement draggedElement, WebElement target){
        Actions act= new Actions(driver);
        act.dragAndDrop(draggedElement,target);
        Utils.LOG.info("Drag element " + draggedElement +" to element " + target);

    }

    public void typeValueInField(String value, String field, Object... fieldArguments) {
        String locator = getLocatorValueByKey(field, fieldArguments);
        WebElement element = driver.findElement(By.xpath(locator));
        element.sendKeys(value);
        Utils.LOG.info("Write text \"" + value + "\" in element " + field);
    }

    public void selectValueFromDropDown(String value, String field, Object... fieldArguments) {
        String locator = getLocatorValueByKey(field, fieldArguments);
        WebElement element = driver.findElement(By.xpath(locator));
        Select dropDownMenu = new Select(element);
        dropDownMenu.selectByVisibleText(value);
        Utils.LOG.info("Select value \"" + value + "\" from drop-down menu " + field);
    }

    public void clearFieldText(String field){
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
        element.sendKeys(Keys.CONTROL+"a");
        element.sendKeys(Keys.BACK_SPACE);
    }

    public void fileUpload(String fileName, String locator){
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
        URL url  = getClass().getClassLoader().getResource(fileName);

        try {
            URI uri = url.toURI();
            File file = new File(uri);
            String path = file.getAbsolutePath();
            element.sendKeys(path);
        }
        catch (URISyntaxException e) {
            System.out.println("URI Syntax Error: " + e.getMessage());
        }

        Utils.LOG.info("Upload: " + fileName + "file");
    }

    public String generateRandomString(int n) {

        String AlphaString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {
            int index = (int)(AlphaString.length() * Math.random());
            sb.append(AlphaString.charAt(index));
        }

        return sb.toString();
    }

    public String buildLocatorForAssertion(String locator, Object... locatorArguments){
        String xpath = getLocatorValueByKey(locator, locatorArguments);
        return xpath;
    }

    //############# WAITS #########

    public void waitForElementVisible(String locatorKey, Object... arguments) {
        Integer defaultTimeout = Integer.parseInt(Utils.getConfigPropertyByKey("config.defaultTimeoutSeconds"));

        Utils.LOG.info("Wait for element " + locatorKey + " to be visible");
        waitForElementVisibleUntilTimeout(locatorKey, defaultTimeout, arguments);
    }

    public void waitForElementVisibleUntilTimeout(String locator, int seconds, Object... locatorArguments) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        String xpath = getLocatorValueByKey(locator, locatorArguments);
        try {
            wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xpath)));
        } catch (Exception exception) {
            Assert.fail("Element with locator: '" + xpath + "' was not found.");
        }
    }

    public void waitForElementClickable(String locatorKey, Object... arguments) {
        Integer defaultTimeout = Integer.parseInt(Utils.getConfigPropertyByKey("config.defaultTimeoutSeconds"));

        Utils.LOG.info("Wait for element " + locatorKey + " to be visible");
        waitForElementClickableUntilTimeout(locatorKey, defaultTimeout, arguments);
    }

    public void waitForElementClickableUntilTimeout(String locator, int seconds, Object... locatorArguments) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        String xpath = getLocatorValueByKey(locator, locatorArguments);
        try {
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
        } catch (Exception exception) {
            Assert.fail("Element with locator: '" + xpath + "' was not found.");
        }
    }

    public void waitForElementPresent(String locator, Object... locatorArguments) {

        Integer defaultTimeout = Integer.parseInt(Utils.getConfigPropertyByKey("config.defaultTimeoutSeconds"));
        WebDriverWait wait = new WebDriverWait(driver, defaultTimeout);
        String xpath = getLocatorValueByKey(locator, locatorArguments);
        Utils.LOG.info("Wait for element " + locator + " to present");
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
        } catch (Exception exception) {
            Assert.fail("Element with locator: '" + xpath + "' was not found.");
        }
    }

    public boolean isElementPresent(String locator, int seconds, Object... locatorArguments) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        String xpath = getLocatorValueByKey(locator, locatorArguments);
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
            return true;
        } catch (Exception exception) {
            Assert.fail("Element with locator: '" + xpath + "' is not present.");
        }
        return false;
    }

    public void waitFor(long timeOutMilliseconds) {
        Utils.LOG.info("Wait for " + timeOutMilliseconds + " milliseconds");
        try {
            Thread.sleep(timeOutMilliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //############# ASSERTS #########

    public void assertElementPresent(String locatorKey, Object... arguments) {
        String xpath = getLocatorValueByKey(locatorKey, arguments);
        Assert.assertNotNull("Searched element is not present", driver.findElement(By.xpath(Utils.getUIMappingByKey(xpath))));
    }

    public void assertNavigatedUrl(String urlKey) {
        String currentURL = getDriver().getCurrentUrl();
        Assert.assertTrue("Navigated URL is not as expected",currentURL.contains(urlKey));
    }

    private String getLocatorValueByKey(String locator, Object[] arguments) {
        return String.format(Utils.getUIMappingByKey(locator), arguments);
    }
}
