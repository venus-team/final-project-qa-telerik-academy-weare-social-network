package com.telerikacademy.WеAre.pages.UnregUser;

import com.telerikacademy.WеAre.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class RegistrationFormPage extends BasePage {
    public RegistrationFormPage(WebDriver driver) {
        super(driver, "registrationFormUrl");
    }

    public void fillInRegistrationForm(String userName, String email, String pass){
        assertLogInFormIsNavigated();
        fillUserName(userName);
        fillEmail(email);
        fillPassword(pass);
        fillConfirmPassword(pass);
    }


    public void fillUserName(String userName){
        actions.waitForElementClickable("registrationFormPage.userNameInput");
        actions.typeValueInField(userName,"registrationFormPage.userNameInput");
    }

    public void fillEmail(String email){
        actions.waitForElementClickable("registrationFormPage.emailInput");
        actions.typeValueInField(email,"registrationFormPage.emailInput");
    }

    public void fillPassword(String pass){
        actions.waitForElementClickable("registrationFormPage.passwordInput");
        actions.typeValueInField(pass,"registrationFormPage.passwordInput");
    }

    public void fillConfirmPassword(String pass){
        actions.waitForElementClickable("registrationFormPage.confirmPasswordInput");
        actions.typeValueInField(pass,"registrationFormPage.confirmPasswordInput");
    }

    public void clickRegisterBnt(){
        actions.waitForElementClickable("registrationFormPage.registerBnt");
        actions.clickElementJS("registrationFormPage.registerBnt");
    }

   //******************** ASSERTIONS *********************

    public void assertLogInFormIsNavigated(){
        actions.waitForElementClickable("registrationFormPage.headerJoinOurCommunity");

    }
}
