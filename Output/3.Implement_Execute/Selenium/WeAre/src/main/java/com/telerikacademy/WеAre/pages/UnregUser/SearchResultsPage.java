package com.telerikacademy.WеAre.pages.UnregUser;

import org.openqa.selenium.WebDriver;
import com.telerikacademy.WеAre.pages.BasePage;

public class SearchResultsPage extends BasePage {

    public SearchResultsPage(WebDriver driver) {
        super(driver, "baseUrl");
    }

    public void selectFirstUserProfileFromResultToReview(){
        actions.waitForElementClickable("resultPage.firstSearchedProfileSeeProfileBnt");
        actions.clickElementJS("resultPage.firstSearchedProfileSeeProfileBnt");
    }


    //********************* ASSERTIONS ******************************

    public void assertResultIsNotEmpty(String result) {
        actions.waitForElementClickable(result);
        actions.assertElementPresent(result);
    }

    public void assertSearchedNamePresent(String serchedTerm){
        actions.waitForElementClickable("resultPage.firstSearchedNameResult", serchedTerm);
    }

    public void assertSearchedProfessionPresent(String serchedTerm){
        actions.waitForElementClickable("resultPage.firstSearchedProfessionResult", serchedTerm);
    }

}
