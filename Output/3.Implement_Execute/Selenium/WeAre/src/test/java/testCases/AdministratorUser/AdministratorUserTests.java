package testCases.AdministratorUser;

import com.telerikacademy.WеAre.pages.UnregUser.HomePage;
import com.telerikacademy.WеAre.pages.UnregUser.SearchResultsPage;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import com.telerikacademy.WеAre.pages.RegisterUser.PersonalProfilePage;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class AdministratorUserTests extends BaseTestAdministratorUser {

    public static final String EDITED_USER = "VenusEdit";
    public static final String USER_FIRST_NAME = "VenusEditEdited";
    public static final String USER_LAST_NAME = "TestEdited";
    public static final String USER_BIRTH_DAY = "02";
    public static final String USER_BIRTH_MONTH = "07";
    public static final String USER_BIRTH_YEAR = "1979";
    public static final String USER_GENDER = "FEMALE";
    public static final String USER_EMAIL = "venusEdited@edit.mail";
    public static final String USER_CITY = "Varna";

    HomePage home = new HomePage(actions.getDriver());
    SearchResultsPage resultsPage = new SearchResultsPage(actions.getDriver());
    PersonalProfilePage personalProfilePage = new PersonalProfilePage(actions.getDriver());

    @Test
    public void SAU_H_001_adminEditUserPersonalInformationWhenValidDataEntered(){
        home.searchAndSubmitByName(EDITED_USER);
        resultsPage.assertSearchedNamePresent(EDITED_USER);
        resultsPage.selectFirstUserProfileFromResultToReview();
        StringBuilder sb = new StringBuilder();
        String userBirthday = sb.append(USER_BIRTH_YEAR).append("-").append(USER_BIRTH_MONTH).append("-").append(USER_BIRTH_DAY).toString();

        personalProfilePage.editPersonalInformation(USER_FIRST_NAME, USER_LAST_NAME, USER_BIRTH_DAY, USER_BIRTH_MONTH , USER_BIRTH_YEAR , USER_GENDER, USER_EMAIL, USER_CITY);

        actions.assertElementPresent("userProfilePage.firstNameAssert", USER_FIRST_NAME);
        actions.assertElementPresent("userProfilePage.secondNameAssert", USER_LAST_NAME);
        actions.assertElementPresent("userProfilePage.birthdayAssert", userBirthday);
        actions.assertElementPresent("userProfilePage.genderAssert", USER_GENDER);
        actions.assertElementPresent("userProfilePage.emailAssert", USER_EMAIL);
        actions.assertElementPresent("userProfilePage.fewWordsAssert", USER_FIRST_NAME);
        actions.assertElementPresent("userProfilePage.cityAssert", USER_CITY);
    }
}
