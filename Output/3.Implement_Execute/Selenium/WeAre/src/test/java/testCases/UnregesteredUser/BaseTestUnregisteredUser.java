package testCases.UnregesteredUser;

import com.telerikacademy.WеAre.UserActions;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public class BaseTestUnregisteredUser {
    UserActions actions = new UserActions();
    @BeforeClass
    public static void setUp(){
        UserActions.loadBrowser("baseUrl");
    }

    @AfterClass
    public static void tearDown(){
        UserActions.quitDriver();
    }
}
