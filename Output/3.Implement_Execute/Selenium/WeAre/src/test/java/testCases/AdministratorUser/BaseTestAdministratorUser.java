package testCases.AdministratorUser;

import com.telerikacademy.WеAre.UserActions;
import com.telerikacademy.WеAre.Utils;
import com.telerikacademy.WеAre.pages.RegisterUser.RegisterUserHomePage;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import com.telerikacademy.WеAre.pages.UnregUser.SignInPage;

public class BaseTestAdministratorUser {
    UserActions actions = new UserActions();
    String adminUsername = Utils.getConfigPropertyByKey("adminUsername");
    String adminPassword = Utils.getConfigPropertyByKey("adminPassword");

    @BeforeClass
    public static void setUp() {
        UserActions.loadBrowser("signinUrl");
    }

    @Before
    public void testSetUp() {
        SignInPage signInPage = new SignInPage(actions.getDriver());
        signInPage.signInUser(adminUsername, adminPassword);
        actions.waitForElementClickable("registerUserHomePage.logoutBnt");

        actions.assertElementPresent("registerUserHomePage.logoutBnt");
    }

    @After
    public void testTearDown(){
        RegisterUserHomePage registerUserHomePage = new RegisterUserHomePage(actions.getDriver());

        registerUserHomePage.logOut();

        actions.waitForElementClickable("registerUserHomePage.logOutAssert");
        actions.assertElementPresent("registerUserHomePage.logOutAssert");
    }

    @AfterClass
    public static void tearDown(){
        UserActions.quitDriver();
    }
}