package testCases.UnregesteredUser;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import com.telerikacademy.WеAre.pages.UnregUser.HomePage;
import com.telerikacademy.WеAre.pages.UnregUser.PublicPostsPage;
import com.telerikacademy.WеAre.pages.UnregUser.RegistrationFormPage;
import com.telerikacademy.WеAre.pages.UnregUser.SearchResultsPage;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class UnregisteredUserTests extends BaseTestUnregisteredUser {
    public static final String USER_NAME = "Venus";
    public static final String USER_PROFESSION = "Designer";

    HomePage home = new HomePage(actions.getDriver());
    SearchResultsPage resultsPage = new SearchResultsPage(actions.getDriver());
    PublicPostsPage publicPostsPage = new PublicPostsPage(actions.getDriver());
    RegistrationFormPage registrationFormPage = new RegistrationFormPage(actions.getDriver());

    @Test
    public void searchInputsVisibleWhenHomePageNavigated(){

        actions.waitForElementClickable("homePage.searchByNameInput");
        actions.waitForElementClickable("homePage.searchByProfessionInput");

        home.assertSearchInputsPresent();
    }

    @Test
    public void SUU_H_001_searchWithoutCriteriaWhenExistUsers(){
        home.searchWithoutTerm();

        resultsPage.assertResultIsNotEmpty("resultPage.firstResult");
    }

    @Test
    public void SUU_H_002_searchByNameWhenUserWithThatNameExists(){
        home.assertSearchInputsPresent();

        home.searchAndSubmitByName(USER_NAME);

        resultsPage.assertSearchedNamePresent(USER_NAME);
    }

    @Test
    public void SUU_H_003_searchByProfessionWhenUserWithThatProfessionExists(){
        home.assertSearchInputsPresent();

        home.searchAndSubmitByProffesion(USER_PROFESSION);

        resultsPage.assertSearchedProfessionPresent(USER_PROFESSION);
    }

    @Test
    public void SUU_H_006_viewLatestPostsWhenClickOnLatestPostsBtn(){
        home.navigateToLatestPostsPage();

        publicPostsPage.assertLatestPostsArePresent();

        actions.assertElementPresent("publicPostsPage.latestPostsPageHeader");
    }

    @Test
    public void SUU_H_007_viewFirstPostFromLatestPostsWhenClickOnExploreThisPostsBtn(){
        SUU_H_006_viewLatestPostsWhenClickOnLatestPostsBtn();

        publicPostsPage.ExploreFirstPostFromPublicPosts();
        actions.waitForElementClickable("publicPostPage.headerExplorePost");

        actions.assertElementPresent("publicPostPage.headerExplorePost");
    }

    @Test
    public void SUU_H_010_registerUserWhenValidDataEntered(){
        home.clickOnRegisterBnt();

        String userName= actions.generateRandomString(6);
        registrationFormPage.fillInRegistrationForm(userName, "VenusTeam@user1.bg", "123456");

        registrationFormPage.clickRegisterBnt();
        actions.waitForElementClickable("confirmRegistrationPage.updateProfileBnt");

        actions.assertElementPresent("confirmRegistrationPage.updateProfileBnt");
    }

    @Test
    public void SUU_U_001_receiveWarningMessageWhenRegisterUserWithInvalidUsername(){
        home.clickOnRegisterBnt();

        registrationFormPage.fillInRegistrationForm("TestUser1", "test1@test.test", "testPassword");

        registrationFormPage.clickRegisterBnt();
        actions.waitForElementClickable("registrationFormPage.warningMessageInvalidUsername");

        actions.assertElementPresent("registrationFormPage.warningMessageInvalidUsername");
    }
}
