package testCases.RegisteredUser;

import com.telerikacademy.WеAre.Utils;
import com.telerikacademy.WеAre.pages.RegisterUser.CreatePostPage;
import com.telerikacademy.WеAre.pages.RegisterUser.OtherUserProfilePage;
import com.telerikacademy.WеAre.pages.RegisterUser.PersonalProfilePage;
import com.telerikacademy.WеAre.pages.RegisterUser.RegisterUserHomePage;
import com.telerikacademy.WеAre.pages.UnregUser.HomePage;
import com.telerikacademy.WеAre.pages.UnregUser.PublicPostsPage;
import com.telerikacademy.WеAre.pages.UnregUser.SearchResultsPage;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class RegisteredUserTests extends BaseTestRegisteredUsers {

    public static final String USER_FIRST_NAME = "Venus";
    public static final String USER_LAST_NAME = "Test";
    public static final String USER_BIRTH_DAY = "14";
    public static final String USER_BIRTH_MONTH = "07";
    public static final String USER_BIRTH_YEAR = "1999";
    public static final String USER_GENDER = "FEMALE";
    public static final String USER_EMAIL = "venus@edit.mail";
    public static final String USER_CITY = "Burgas";

    RegisterUserHomePage homePage = new RegisterUserHomePage(actions.getDriver());
    PersonalProfilePage personalProfilePage = new PersonalProfilePage(actions.getDriver());
    HomePage unregisterUserHomePage = new HomePage(actions.getDriver());
    SearchResultsPage resultsPage = new SearchResultsPage(actions.getDriver());
    OtherUserProfilePage otherUserProfilePage = new OtherUserProfilePage(actions.getDriver());
    CreatePostPage createPostPage = new CreatePostPage(actions.getDriver());
    PublicPostsPage publicPostsPage = new PublicPostsPage(actions.getDriver());

    String fileToUpload = Utils.getConfigPropertyByKey("fileUpload");

    //********************* USER PROFILE TESTS****************************
    @Test
    public void SRU_H_002_editUserPersonalInformationWhenValidDataEntered(){
        homePage.navigateToPersonalProfile();
        StringBuilder sb = new StringBuilder();
        String userBirthday = sb.append(USER_BIRTH_YEAR).append("-").append(USER_BIRTH_MONTH).append("-").append(USER_BIRTH_DAY).toString();

        personalProfilePage.editPersonalInformation(USER_FIRST_NAME, USER_LAST_NAME, USER_BIRTH_DAY, USER_BIRTH_MONTH , USER_BIRTH_YEAR , USER_GENDER, USER_EMAIL, USER_CITY);

        actions.assertElementPresent("userProfilePage.firstNameAssert", USER_FIRST_NAME);
        actions.assertElementPresent("userProfilePage.secondNameAssert", USER_LAST_NAME);
        actions.assertElementPresent("userProfilePage.birthdayAssert", userBirthday);
        actions.assertElementPresent("userProfilePage.genderAssert", USER_GENDER);
        actions.assertElementPresent("userProfilePage.emailAssert", USER_EMAIL);
        actions.assertElementPresent("userProfilePage.fewWordsAssert", USER_FIRST_NAME);
        actions.assertElementPresent("userProfilePage.cityAssert", USER_CITY);

    }

    @Test
    public void SRU_H_003_editUserProfessionalInformationWhenValidDataEntered() {
        homePage.navigateToPersonalProfile();

        personalProfilePage.editProfessionalInformation("Accountant");
        actions.waitForElementClickable("userProfilePage.professionAssert", "Accountant");

        actions.assertElementPresent("userProfilePage.professionAssert", "Accountant");
    }

    @Test
    public void SRU_H_004_editUserServicesInformationWhenValidDataEntered() {
        homePage.navigateToPersonalProfile();

        personalProfilePage.editServicesInformation("I can do a lot of things" , "5");
        actions.waitFor(100);

        actions.assertElementPresent("userProfilePage.skillAssert", "I can do a lot of things");
        actions.assertElementPresent("userProfilePage.availabilityAssert", "5");
    }

    @Test
    public void SRU_H_005_editUserPersonalInfoAndSafetyWhenValidDataEntered() {
        homePage.navigateToPersonalProfile();

        personalProfilePage.editPictureAndSafetyInformation(fileToUpload , "public");

        actions.assertElementPresent("userProfilePage.pictureAssert");

    }

    //*************************** CONNECTIONS TESTS ***********************************

    @Test
    public void SRU_H_011_sendConnectionRequestWhenNotConnected() {
        unregisterUserHomePage.searchAndSubmitByName("VenusEdit");
        resultsPage.selectFirstUserProfileFromResultToReview();

        otherUserProfilePage.clickOnConnectBnt();

        actions.assertElementPresent("otherUserProfilePage.sentConnectionRequestMessage");
    }

    @Test
    public void SRU_H_012_acceptConnectionRequestWhenPending(){
        homePage.navigateToPersonalProfile();
        personalProfilePage.clickOnNewFriendsRequestsBnt();


        personalProfilePage.clickOnFirstPendingRequestAcceptBnt();

        actions.assertElementPresent("userProfilePage.approvedRequestMsg");
    }

    @Test
    public void SRU_H_014_disconnectFromUserWhenConnected(){
        unregisterUserHomePage.searchAndSubmitByName("adminVenus");
        resultsPage.selectFirstUserProfileFromResultToReview();

        otherUserProfilePage.clickOnDisconnectBnt();

        actions.assertElementPresent("otherUserProfilePage.connectBnt");
    }

    //***************************** POSTS TESTS *********************************

    @Test
    public void SRU_H_017_createPostWhenValidDataEntered(){
        homePage.navigateToCreateNewPostPage();
        String postMessage = actions.generateRandomString(17);

        createPostPage.createPost("Public post" , postMessage , fileToUpload);

        actions.assertElementPresent("latestPostsPage.createdPostText", postMessage);
    }

    @Test
    public void SRU_H_018_likePostWhenThereAreUnlikedPosts(){
        unregisterUserHomePage.navigateToLatestPostsPage();
        publicPostsPage.browseAllPublicPosts();
        String initialCountText = actions.getElementText("publicPostsPage.likeCountAsText");
        String initialCountTextSubstring = initialCountText.substring(7);
        int initialCount = Integer.parseInt(initialCountTextSubstring);

        publicPostsPage.likePost();
        actions.waitForElementVisible("publicPostsPage.dislikePostBnt");
        String countText = actions.getElementText("publicPostsPage.likeCountAsText");
        String countTextSubstring = countText.substring(7);
        int count = Integer.parseInt(countTextSubstring);

        Assert.assertEquals("Likes count is not as expected",initialCount+1, count);
        actions.assertElementPresent("publicPostsPage.dislikePostBnt");
    }

    @Test
    public void SRU_H_019_dislikePostWhenThereAreUnlikedPosts(){
        unregisterUserHomePage.navigateToLatestPostsPage();
        publicPostsPage.browseAllPublicPosts();

        publicPostsPage.dislikePost();
        actions.waitForElementVisible("publicPostsPage.likePostBnt");

        actions.assertElementPresent("publicPostsPage.likePostBnt");
    }

    //**************************** COMMENTS TESTS ***************************

    @Test
    public void SRU_H_020_createCommentWhenValidDataEntered(){
        unregisterUserHomePage.navigateToLatestPostsPage();
        publicPostsPage.browseAllPublicPosts();
        publicPostsPage.ExploreFirstPostFromPublicPosts();
        String message = actions.generateRandomString(10);

        publicPostsPage.postComment(message);

        publicPostsPage.showComments();
        String creatorLocator = actions.buildLocatorForAssertion("publicPostsPage.commentFromUser", loggedUserName);
//        actions.assertElementPresent(creatorLocator);
//        String commentLocator = actions.buildLocatorForAssertion("publicPostsPage.commentText", message);
        actions.assertElementPresent("publicPostsPage.commentText", message);


    }

    @Test
    public void SRU_H_021_likeCommentWhenCommentExistAndNotLiked(){
        unregisterUserHomePage.navigateToLatestPostsPage();
        publicPostsPage.browseAllPublicPosts();
        publicPostsPage.ExploreFirstPostFromPublicPosts();
        publicPostsPage.showComments();
        actions.waitForElementVisible("publicPostsPage.likeCountAsText");
        String initialCountText = actions.getElementText("publicPostsPage.likeCountAsText");
        String initialCountTextSubstring = initialCountText.substring(7);
        int initialCount = Integer.parseInt(initialCountTextSubstring);

        publicPostsPage.likeComment();

        actions.waitForElementClickable("publicPostsPage.likeCountAsText");
        String countText = actions.getElementText("publicPostsPage.likeCountAsText");
        String countTextSubstring = countText.substring(7);
        int count = Integer.parseInt(countTextSubstring);
        Assert.assertEquals("Likes count is not as expected",initialCount+1, count);
        actions.assertElementPresent("publicPostsPage.dislikeCommentBnt");

    }

    @Test
    public void SRU_H_022_dislikeCommentWhenCommentExistAndLiked(){
        unregisterUserHomePage.navigateToLatestPostsPage();
        publicPostsPage.browseAllPublicPosts();
        publicPostsPage.ExploreFirstPostFromPublicPosts();
        publicPostsPage.showComments();
        actions.waitForElementVisible("publicPostsPage.likeCountAsText");
        String initialCountText = actions.getElementText("publicPostsPage.likeCountAsText");
        String initialCountTextSubstring = initialCountText.substring(7);
        int initialCount = Integer.parseInt(initialCountTextSubstring);

        publicPostsPage.dislikeComment();

        actions.waitForElementVisible("publicPostsPage.likeCountAsText");
        String countText = actions.getElementText("publicPostsPage.likeCountAsText");
        String countTextSubstring = countText.substring(7);
        int count = Integer.parseInt(countTextSubstring);
        Assert.assertEquals("Likes count is not as expected",initialCount-1, count);
        actions.assertElementPresent("publicPostsPage.likeCommentBnt");
    }

}
