git clone --single-branch --branch main	 https://gitlab.com/venus-team/final-project-qa-telerik-academy-weare-social-network.git
cd "final-project-qa-telerik-academy-weare-social-network\Output\3.Implement_Execute\Selenium\WeAre"
mvn -Dtest=UnregisteredUserTests.java test > resultsUnregisterUsers.log
mvn -Dtest=RegisteredUserTests test > resultsRegisteredUser.log
mvn -Dtest=AdministratorUserTests test > resultsAdministratorUser.log
