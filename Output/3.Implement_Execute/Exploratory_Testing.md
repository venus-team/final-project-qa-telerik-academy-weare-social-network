|Exploratory testing charter | |
|----------------------------|-|
|Testers: | Aneliya Gishina & Elena Matuska-Malinova|
|Date:  | 06.07.2021|
|Duration execution: | 4 man-hours|
|Purpose, actors, setup, mission and tactics | To get familiar with delivered functionalities and features and to identify issues in the most common User Scenarios, also covering boundary values and corner cases and overcome the limitations of scripted testing. Actors are main users in application: unregistered user, registered user and administrator|
|Test ideas & Test data | Check all "Happy paths" with created users Aneliya and Elena|
|Test object and references | Verify that the functionality of the website works according to the specifications|
|Scope |Website functionalities of the product for the roles unregistered user, registered user and administrator; User Interface|



# Test log  

## 1. Home page  

|Inputs Actions | Expected output | Actual output | Observation(s) (Defect-id)|
|---------------|-----------------|---------------|---------------------------|
|Click Search Button without fill input fields | View all registered users | View all registered users | |
|Click WEare logo | Go to home page | Go to home page | |
|Click "Register" button in main menu | Open "Register" page | Open "Register" page | Missing elements of main menu. Missing footer menu. Incorect breadcrumbs - "HOME > LOGIN"|
|Click "Sign In" button in main menu | Open "Log In" page | Open "Log In" page | Missing element of main menu|
|Click "Home" in main menu| Go to home page | Go to home page | |
|Click "About Us" in main menu | View information about project | View information about project | The element "About Us" is missing in main menu |
|Click "Latest Post" in main menu | View posts chronologically ordered | View list of users profile | Missing Register and Sign In Buttons |
|Click blue dots in the bottom of "HOW DOES IT WORK?" section | Scroll reiews | Scroll reiews | |
|Click "Register" button in section "Do what you love, get what you need" | Open "Register" page | Open "Register" page | Missing elements of main menu. Missing footer menu. Incorect breadcrumbs - "HOME > LOGIN"|
|Click twiter icon in bottom menu | Open twiter profile | Go to top | Doesn't open twiter profil. |
|Click facebook icon in footer menu | Open facebook profile | Go to top | Doesn't open facebook profil. |
|Click instagram icon in footer menu | Open instagram profile | Go to top | Doesn't open instagram profil. |
|Click "How it work?" in section "Useful links" in bottom menu | Open "How it work?" page | Open "About us" page | Missing "How it work?" page or broken/incorect link | 
|Click "About us" in section "Useful links" in footer menu | Open "About us" page | Open "About us" page | In main menu of "About us" page missing "about us" link | 
|Click "See All Posted Services" in section "Useful links" in footer menu | Open "Posts" page | Open "Posts" page | |
|Click "Sign In" in section "Account" in footer menu | Open "Log In" page | Open "Log In" page | Missing element of main menu|
|Click "Create Account" in section "Account" in footer menu | Open "Register" page | Open "Register" page | Missing elements of main menu. Missing bottom menu. Incorect breadcrumbs - "HOME > LOGIN"|
|Click on the phone numbers in section "Have a Questions?" in footer menu | Phone number is clickable | Go to top | Phone numbers aren't clickable |
|Click on the email in section "Have a Questions?" in foter menu | Open mail client | Go to top | Function "Write email" doesn't present |

## 2. About Us page

|Inputs Actions | Expected output | Actual output | Observation(s) (Defect-id)|
|---------------|-----------------|---------------|---------------------------|


## 3. Posts page

|Inputs Actions | Expected output | Actual output | Observation(s) (Defect-id)|
|---------------|-----------------|---------------|---------------------------|
|View user post| Visibility settings of the post(public/private) are not displayed| Visibility settings of the post(public/private) are displayed| |
|View user post| Date and time of creation of the post are in user time zone | Date and time of creation of the post are not in user time zone| |

## 4. Register page  

|Inputs Actions | Expected output | Actual output | Observation(s) (Defect-id)|
|---------------|-----------------|---------------|---------------------------|
|Fill in all fields in registration form with valid data | Message "You are successfully registered. Please update your profile" together with Sign in form is displayed| Only "please update your profile" is displayed| |
|Fill in Username field with name containing number in registration form and all other fields with valid data| Message "Username require only characters" is displayed| Message "username requires no whitespaces, only character" is displayed| |

## 5. Sign In page  

|Inputs Actions | Expected output | Actual output | Observation(s) (Defect-id)|
|---------------|-----------------|---------------|---------------------------|
|Fill in all fields in registration form with valid data | Message "You are successfully registered" is displayed| "please update your profile" is displayed| |
