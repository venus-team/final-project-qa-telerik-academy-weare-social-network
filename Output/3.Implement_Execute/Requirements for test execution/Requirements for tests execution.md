## Selenium Webdriver

**To be able to run automated test on a http://localhost:8081/ stage you have to:** 

1. Follow instructions for Docker instalation in [Docker instructions](https://drive.google.com/file/d/1zCzuR0_Pqj7A5855RvPwC4bZ5GkTy_m2/view?usp=sharing) file. **Docker-compose.yml** file can be found [here](https://gitlab.com/venus-team/final-project-qa-telerik-academy-weare-social-network/-/tree/main/Output/3.Implement_Execute/Requirements%20for%20test%20execution);

2. To load Test Data and run API tests you need Postman integration with Newman(further information later in this document);

3. Download the files found [here](https://gitlab.com/venus-team/final-project-qa-telerik-academy-weare-social-network/-/tree/main/Output/2.Analyse%20and%20Design/TestData%20load) in a folder on your computer and run **TestDataLocalLoad.bat** file from that folder.


**How to select a test stage:**

The selection of stage is done by changing the active lines in: [config.properties](https://gitlab.com/venus-team/final-project-qa-telerik-academy-weare-social-network/-/blob/main/Output/3.Implement_Execute/Selenium/WeAre/src/test/resources/config.properties)


**There are two (2) options for a test browser:**


- Mozzila FireFox  
- Google Chrome  

**How to select a testing browser:**

The selection of browser is done by changing the active lines in: [CustomWebDriverManager.java](https://gitlab.com/venus-team/final-project-qa-telerik-academy-weare-social-network/-/blob/main/Output/3.Implement_Execute/Selenium/WeAre/src/main/java/com/telerikacademy/W%D0%B5Are/CustomWebDriverManager.java)


**Running Maven tests**  

Download and run the [StartAutomated_UI_tests.bat](https://gitlab.com/venus-team/final-project-qa-telerik-academy-weare-social-network/-/tree/main/Output/3.Implement_Execute/Selenium/WeAre) file in a folder on your computer. This will download the whole project and run Maven tests.


## Postman integration with Newman

**Prerequisites:** Node.js installed

Install Newman in command line/terminal

```npm install -g newman```

Install newman-reporter-htmlextra

```npm install -g newman-reporter-htmlextra```

**Running API tests**  

Download the files found [here](https://gitlab.com/venus-team/final-project-qa-telerik-academy-weare-social-network/-/tree/main/Output/3.Implement_Execute/Postman) in a folder on your computer and run the following files from that folder to generate newman reports :

|Test stage | File |
|-----------|------|
|http://localhost:8081/|RunWebServicesTests.bat|
|http://164.138.216.247:8082/|RunWebServicesTests_production.bat|

