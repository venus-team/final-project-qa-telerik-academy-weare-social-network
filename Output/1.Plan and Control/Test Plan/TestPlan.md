# **Test Plan for WEare Social Network prepared on 03.07.2021 by:**
- Aneliya Gishina
- Elena Matuska-Malinova

## **Table of contents**
1. [INTRODUCTION](#1-introduction)    
2. [OBJECTIVES AND TASKS](#2-objectives-and-tasks)
3. [SCOPE](#3-scope)   
4. [TESTING STRATEGY](#4-testing-strategy)
5. [HARDWARE/ENVIRONMENT REQUIREMENTS](#5-hardwareenvironment-requirements)
6. [SCHEDULE](#6-schedule)
7. [ENTRY CRITERIA](#7-entry-criteria)
8. [EXIT CRITERIA](#8-exit-criteria)
9. [TEST DELIVERABLES](#9-test-deliverables)

---

## 1. **INTRODUCTION:**   
The application under test is [WEare Social Network](http://164.138.216.247:8082/). This thematical social network is a professional network, where people can exchange experiance and services and allows users to connect and search for each other.
There are two types of users - a regular user and an administrative user.
A regular user can create posts, leave comments, like/unlike posts, gets a feed of the latest posts of their connections, connects with other users.
An administrative user has the functionalities of the regular one and he can additionally edit and delete profiles, comments and posts.
The purpose of this project is to verify that the delivered software meets the user requirements, is production ready and is ensuring the stability by providing automated test.

## 2. **OBJECTIVES AND TASKS**
- The **objective** of the tests is to:  
    - verify that the functionality of the website works according to the specifications;   
    - ensure bugs/issues are identified and properly logged.   
   
  
- The **tasks** to be performed include:   
    - Exploratory Testing and preparation of a Coverage Report;   
    - Development of suites of Test Cases;   
    - Manual testing of the UI;
    - Preparation of automation tests for at least the happy paths using Selenium WebDriver;   
    - Web Services testing through REST API automated tests in Postman;
    - Non-functional testing;
    - Bug/Issue logging – performed in GitLab;
    - Documentation with instructions for the script execution;   
    - Preparation of a Test Report;  
  
The team members have separated the tasks between them in an approximately equal efforts by each of them.   


## 3. SCOPE
The software components that fall under the scope of this plan are:
- Website functionalities (with focus on “happy paths”) of the product for the roles:   
    - Unregistered user;   
    - Registered user;   
    - Administrator.   
- User Interface;
- Web Services testing through REST API requests.


### 3.1. Features to be tested:  
The list of the features which are going to bе tested is: 
- Application home page - landing page with links to the login and registration forms, profile search and public feed – from anonymous user, registered user and admin perspective;
- Registration form – verifying fields, public and private information, upload of picture;
- Login form - verifying fields;
- Search profile - posts, categories, users;
- Feed – posts - basic requirements and optional, comments, likes, visibility of posts - private and public;
- Admin functionalities - user’s and additional functionalities - add/delete profiles, posts, comments, add categories;
- Connections - interactions with other users;
- Manage user profile – changes in profiles.

### 3.2. Features not to be tested:  
The list of the features which are not going to be tested is:  
- Mobile testing - out of our scope;  
- Network security testing - out of our scope.

Black box testing techniques will be used.   

## 4. TESTING STRATEGY
The strategy for covering the testing scope includes separation of the functionalities in suites and assigning the development of test cases and automation to different members of the team. Test cases priority will be:
|High|
|-------------------------------------| 
|The test cases that need to be executed first are the ones attached to high priority and high severity.|

|Medium|
|---------------------------------------------------------|
|Medium level test cases determine that the crucial information available on the product is correctly conveyed. These are test cases with low priority but with high severity.|

|Low|
|-----------------------------------------------------------------|
|These test cases will not impact the functionality or the quality of the product to be released and need not necessarily be executed in case of a time crunch.|


Everything will be commited to a git repository in GitLAb and merge approval rule will be implemented.
The team will have daily meetings to verify the schedule is followed and no blocking test issues are active.  

### 4.1. Component/Unit testing
- This step is already performed by the development team and is not in the scope of the test team assignment.

### 4.2. Integration Testing

- This step is already performed by the development team, when new modules were developed and added to the system. QA Team may also perform integration testing to ensure that modules integrate without defect issues from users’ perspective. Such testing will be performed using black-box testing method with manual and automated tests

### 4.3. System Testing
  The testing effort of the QA team will be focused on System level where end-to-end user scenarios will be tested. The system will be tested for its performance and compliance with customer requirements for intuitive, simple and user-friendly UI. The following types of testing will be performed:

- **Exploratory Testing** - Exploratory testing will be performed to get familiar with delivered functionalities and features. It will help to identify issues in the most common User Scenarios, also covering boundary values and corner cases and overcome the limitations of scripted testing. It is an approach to testing whereby the tester dynamically designs and execute tests based on their knowledge, exploration of the test item and the results of previous tests.
- **Functional testing** - The purpose is to ensure that all features will function according to customer specifications.
- [ ] Black box technique like Equivalence partitioning, Boundary value analysis, Decision tables and Classification trees will be conducted;
- [ ] Happy paths will be automated;
- **Usability testing** - The goal is to find confusing or misleading areas that can decrease the user experience;
- [ ] Manual testing technique will be conducted;

### 4.4. Acceptance testing
- The purpose is to be confirmed that the system is ready for operational use. During acceptance test, end-users (customers) of the system compare the system to its initial requirements. In this case the user acceptance testing will be performed by the testing team and report will be prepared with deficiencies of the site functionalities;


## 5. HARDWARE/ENVIRONMENT REQUIREMENTS:  
In this section are listed the requirements that are necessary before execution of the Tests cases.  
- The testing will be conducted on the following platforms:  
--- 
    Desktop: Windows 10 Pro, version: 21H1, 64-bit;  
       - processor: Intel(R) Core(TM) i3-5005U CPU @ 2.00GHz
       - RAM: 4,00 GB  
---    
    Desktop: Windows 10 Pro, version: 20H2, 64-bit;    
       - processor: 11th Gen Intel(R) Core(TM) i5-1135G7 @ 2,40GHz
       - RAM: 16 GB
---

- Browsers:   
    - Chrome version: 91.0.4472.124 (64-bit);  
    - Firefox version: 89.0.2 (64-bit);  
    - Edge version: 91.0.864.64 (64-bit);  
- Testing environment:
    - JDK 11;
    - IntelliJ IDEA;
    - Postman;
    - Apache Maven ver 3.6.0 or later;
    - Newman reporting tool;
    - Docker;
    
## 6. SCHEDULE
The project will be executed in four weeks period. This include:

|First week:|28.06-4.07|
|-------------|--------| 
    - Read and analyze documentation- 4 man-hours;  
    - Create GIT repo & Trello Board- 2  man-hours;
    - Test plan creation - 5 man-hours;   
    - Test cases Template creation - 0,5 man-hours;
    - Issues Template creation - 0,5 man-hours;
    - Exploratory testing - 4 man-hours; 
    - Exploratory testing report creation - 2 man-hours;

|Second week:|5.07-11.07|
|------------|----------|
    - Draft Test cases;
    - Creation of Name decision document for issues and test cases- 8 man-hours;
    - Test cases creation - 6 man-hours;  
    - Setup Test automation environment - 2 man-hours;
    - Test development configuration/setup - 2 man-hours;  

|Third week:|12.07-18.07|
|-----------|-----------|
    - Execution of all Test cases - 6 man-hours;
    - Implementation of REST API Automated test - 12 man-hours; 
    - Implementation of UI test - 12 man-hours;
    - Implementation of automated test execution - 2 man-hours.

|Forth week:|19.07-25.07|
|-----------|-----------| 
    - Test Summary Reports creation - 4 man-hours; 
    - Test Incident Reports creation - 4 man-hours;
    - Finalization of tests execution and reports - 6 man-hours;
    

## 7. ENTRY CRITERIA:  
The entry criteria defines the point at which testing will start. The following entry criteria are defined:  
- Defined and Approved Requirements;    
- Test environment is available and ready for use;  
- Testable code is available.

## 8. EXIT CRITERIA:  
The exit criteria defines the point at which testing will stop. The following exit criteria are defined:   
- Achieved complete Functional Coverage to a level not less than 95%;  
- All blocking and high bugs are fixed and verified;
- The time expired at 01.08.2021 23:59:59

## 9. TEST DELIVERABLES:  
After the testing phase the deliverables will include:   
- Test Case Specification;
- Test Incident/Bug Report;
- Test Summary Report;
- Test Input Data;
- Automated tests code/repository link;
- Script for execution of automated tests;
- A document with required prerequisites to run the tests;
- A document with a short description how to run the tests, how to filter them, etc;
- High-level test cases;

- **The following documents support the Test plan:**
    - [Telerik_Academy_Project_Social_Network.docx](https://drive.google.com/file/d/14yJE1sscoU3ZyW9B8mXWp5nmQly4B8yH/view?usp=sharing)
    - [Telerik_Academy_Alpha28QA_Project.pdf](https://drive.google.com/file/d/1AYnOKwpl_mP4owM0trvlgQriKs3EOTCV/view?usp=sharing)
