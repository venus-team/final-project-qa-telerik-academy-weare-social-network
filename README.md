# Testing [WEare Social Network](http://164.138.216.247:8082/) #


**QA Team **"VENUS"** with members:** 
| <img src="Images/Neli.jpg" alt="figure" width="70px" style="margin-top: 10px;"/> Aneliya Gishina |<img src="Images/ElenaMatuska.jpg" alt="figure" width="70px" style="margin-top: 10px;"/> Elena Matuska-Malinova |
|-----------------|-------------------------|


  
###  **1. REQUIREMENTS OF THE PROJECT:**   

<img src="Images/WEare.JPG" alt="figure" width="1100px" style="margin-top: 10px;"/>

The application under test is [WEare Social Network](http://164.138.216.247:8082/). It was developed by the students in the Java cohort at Telerik Academy. This thematical social network is a professional network, where people can exchange experiance and services and allows users to connect and search for each other. More detailed information about the technical specifications of the application can be found [here](https://drive.google.com/file/d/1holcSdzEC_XY4KTkfdmFZbTYa7lwV2K1/view?usp=sharing).

**The QA project task is to verify that the delivered software meets the user requirements, it is production ready and to ensure stability by providing automated tests. More detailed requirements can be found [here](https://drive.google.com/file/d/1HLj3-60q6auaY5awu_xT6U_aTpTirC3c/view?usp=sharing)**

### **2. TEST PLAN:**

Test plan can be found here: [TEST PLAN](https://gitlab.com/venus-team/final-project-qa-telerik-academy-weare-social-network/-/blob/main/Output/1.Plan%20and%20Control/Test%20Plan/TestPlan.md)

### **3. TEST CASES:**

A table with test cases can be found here: [TEST CASES](https://drive.google.com/file/d/1f0an_ZWwt-xFVdWlZ9hzWOXY5mk5T0W5/view?usp=sharing)

### **4. EXPLORATORY TESTING:**

Report can be found [here](https://gitlab.com/venus-team/final-project-qa-telerik-academy-weare-social-network/-/blob/main/Output/3.Implement_Execute/Exploratory_Testing.md) 

### **5. AUTOMATED TESTS:**

A document with required prerequisites and how to run test can be found [here](https://gitlab.com/venus-team/final-project-qa-telerik-academy-weare-social-network/-/blob/main/Output/3.Implement_Execute/Requirements%20for%20test%20execution/Requirements%20for%20tests%20execution.md)

- **[Automated UI tests](https://gitlab.com/venus-team/final-project-qa-telerik-academy-weare-social-network/-/tree/main/Output/3.Implement_Execute/Selenium/WeAre)**
- **[Automated API tests](https://gitlab.com/venus-team/final-project-qa-telerik-academy-weare-social-network/-/tree/main/Output/3.Implement_Execute/Postman)**

### **6. REPORTS:**
**[Test report](https://drive.google.com/file/d/1zDS_ZjbU3GCDKYoAu1Dk-w7TxksNx-gu/view?usp=sharing)**

